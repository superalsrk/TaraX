TaraX(正在开发中)
===

根据原始csv生成GEXF文件的程序

用于在展示微博转发关系等场景中使用, 最终可以生成如下图表

![](http://7jptw8.com1.z0.glb.clouddn.com/gephi/weibo.png)




### 接口定义

1. http://localhost:8080/doc 基本介绍与文档
2. http://localhost:8080/api/submit POST文件
3. http://localhost:8080/dl/gexf/xxxx 只包含layout文件
4. http://localhost:8080/dl/zip/xxxx 一个压缩包,可以看图
5. http://localhost:8080/status/xxxx 任务状态


### 服务器状态

1. http://localhost:8080/env
2. http://localhost:8080/metrics
3. http://localhost:8080/health
