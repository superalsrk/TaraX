package cn.miaozhen.tarax;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TaraxApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaraxApplication.class, args);
    }
}
