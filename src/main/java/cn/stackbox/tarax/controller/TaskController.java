/**
 * Project Name:tarax
 * Package Name:com.miaozhen.social.controller 
 * Date: 2015-11-27 13:21
 * Copyright (c) 2015,  www.miaozhen All Rights Reserved.
 *
 */
package cn.stackbox.tarax.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * ClassName: TaskController <br/>
 * Reason: SomeReason <br/>
 * Date: 2015-11-27 13:21
 *
 * @author 吕思佳
 */

@Controller
public class TaskController {

    @RequestMapping("/home")
    @ResponseBody
    public String home() {
        return "home";
    }

    @RequestMapping("/api/submit")
    @ResponseBody
    public String submit() {
        return "submit";
    }

    @RequestMapping("/api/status")
    @ResponseBody
    public String status() {
        return "status";
    }
    
}
