/**
 * Project Name:tarax
 * Package Name:com.miaozhen.social.controller 
 * Date: 2015-11-27 13:11
 * Copyright (c) 2015,  www.miaozhen All Rights Reserved.
 *
 */
package cn.stackbox.tarax.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * ClassName: DownloadController <br/>
 * Reason: SomeReason <br/>
 * Date: 2015-11-27 13:11
 *
 * @author 吕思佳
 */
@Controller
@RequestMapping("/dl")
public class DownloadController {


    @RequestMapping("/gexf")
    @ResponseBody
    public String gexf() {
        return "gexf";
    }


    @RequestMapping("/zip")
    @ResponseBody
    public String zip() {
        return "zip";
    }
}
